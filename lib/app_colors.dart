import 'package:flutter/material.dart';

class AppColors {
  static const base = Color(0xFFE7E7E7);
  static const darkText = Color(0xFF343434);
  static const brightText = Color(0xFF909090);
  static const sliderInactiveTrackColor = Color(0xFFECECEC);
  static const playbackControlButton = Color(0xFFDADADA);
}
