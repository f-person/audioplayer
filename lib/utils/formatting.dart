class FormattingUtils {
  static String formatDuration(Duration duration, {bool showHours = false}) {
    final hours = duration.inHours;
    final minutes = duration.inMinutes.remainder(60);
    final seconds = duration.inSeconds.remainder(60);

    return '${showHours ? '$hours:' : ''}' '${minutes < 10 ? '0' : ''}$minutes:' '${seconds < 10 ? '0' : ''}$seconds';
  }
}
