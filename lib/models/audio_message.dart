class AudioMessage {
  const AudioMessage({
    required this.title,
    required this.creationDate,
    required this.transcript,
    required this.duration,
  });

  final String title;
  final DateTime creationDate;
  final String transcript;
  final Duration duration;
}
