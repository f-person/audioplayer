import 'package:audioplayer/app_colors.dart';
import 'package:audioplayer/models/audio_message.dart';
import 'package:audioplayer/utils/formatting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'message_slider.dart';

class PlaybackControls extends HookWidget {
  const PlaybackControls({required this.message});

  final AudioMessage message;

  @override
  Widget build(BuildContext context) {
    final playback = useState<double>(0.0);
    final playbackDuration = Duration(
      milliseconds: playback.value == 0.0 ? 0 : (message.duration.inMilliseconds * playback.value).toInt(),
    );

    final showHours = message.duration.inHours > 0;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: MessageSlider(
            value: playback.value,
            onChanged: (value) => playback.value = value,
            onChangeEnd: (value) {
              // TODO start audio playback from here
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: DefaultTextStyle(
            style: TextStyle(color: AppColors.brightText),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(FormattingUtils.formatDuration(playbackDuration, showHours: showHours)),
                Text(FormattingUtils.formatDuration(message.duration, showHours: showHours)),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
