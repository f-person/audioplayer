import 'package:flutter/material.dart';

class TranscriptGradient extends StatelessWidget {
  const TranscriptGradient({required this.begin, required this.end});

  final Alignment begin;
  final Alignment end;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Container(
        height: 30,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: begin,
            end: end,
            colors: [Colors.white, Colors.white.withOpacity(0)],
          ),
        ),
      ),
    );
  }
}
