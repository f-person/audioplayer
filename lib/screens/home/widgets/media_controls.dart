import 'package:audioplayer/app_colors.dart';
import 'package:audioplayer/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_svg/svg.dart';

class MediaControls extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final isPlaying = useState<bool>(false);

    return Row(
      children: [
        Expanded(child: SizedBox()),
        Row(
          children: [
            IconButton(
              color: AppColors.playbackControlButton,
              iconSize: 32,
              icon: Icon(Icons.forward_30),
              onPressed: () {},
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: IconButton(
                color: Theme.of(context).primaryColor,
                iconSize: 48,
                onPressed: () {
                  isPlaying.value = !isPlaying.value;
                },
                icon: Icon(isPlaying.value ? Icons.pause_rounded : Icons.play_arrow_rounded),
              ),
            ),
            IconButton(
              color: AppColors.playbackControlButton,
              iconSize: 32,
              icon: Icon(Icons.replay_30),
              onPressed: () {},
            ),
          ],
        ),
        Expanded(
          child: PopupMenuButton(
            padding: const EdgeInsets.all(0.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            iconSize: 32,
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).primaryColor,
            ),
            itemBuilder: (context) {
              Widget buildItem({
                required String svgIconPath,
                required String titleText,
              }) {
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SvgPicture.asset(
                      svgIconPath,
                      width: 24,
                      height: 24,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(width: 12),
                    Text(titleText),
                  ],
                );
              }

              return [
                PopupMenuItem(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: buildItem(svgIconPath: AppAssets.speechBubble, titleText: 'Перейти к сообщению'),
                ),
                PopupMenuItem(
                  child: buildItem(svgIconPath: AppAssets.add, titleText: 'Создать задачу'),
                ),
                PopupMenuItem(
                  child: buildItem(svgIconPath: AppAssets.reply, titleText: 'Переслать'),
                ),
                PopupMenuItem(
                  child: buildItem(svgIconPath: AppAssets.download, titleText: 'Скачать'),
                ),
              ];
            },
          ),
        ),
      ],
    );
  }
}
