import 'package:audioplayer/app_colors.dart';
import 'package:flutter/material.dart';

class MessageSlider extends StatelessWidget {
  const MessageSlider({required this.value, required this.onChanged, required this.onChangeEnd});

  final double value;
  final ValueChanged<double> onChanged;
  final ValueChanged<double> onChangeEnd;

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderThemeData(
        trackHeight: 2,
        inactiveTrackColor: AppColors.sliderInactiveTrackColor,
        thumbShape: RoundSliderThumbShape(
          enabledThumbRadius: 6,
        ),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 16),
      ),
      child: Slider(
        value: value,
        onChanged: onChanged,
        onChangeEnd: onChangeEnd,
      ),
    );
  }
}
