import 'package:audioplayer/app_colors.dart';
import 'package:audioplayer/models/audio_message.dart';
import 'package:audioplayer/screens/home/widgets/transcript_gradient.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MessageTranscript extends HookWidget {
  const MessageTranscript({required this.message});

  final AudioMessage message;

  @override
  Widget build(BuildContext context) {
    final isTranscriptShown = useState<bool>(false);
    double transcriptMaxHeight;

    if (!isTranscriptShown.value) {
      transcriptMaxHeight = 0.0;
    } else if (MediaQuery.of(context).size.height < 600) {
      transcriptMaxHeight = MediaQuery.of(context).size.height * 0.3;
    } else {
      transcriptMaxHeight = MediaQuery.of(context).size.height * 0.5;
    }

    return Column(
      children: [
        AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOut,
          constraints: BoxConstraints(maxHeight: transcriptMaxHeight),
          margin: EdgeInsets.only(
            bottom: isTranscriptShown.value ? 16 : 0,
            top: isTranscriptShown.value ? 0 : 16,
          ),
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: SelectableText(
                    message.transcript,
                    style: TextStyle(
                      color: AppColors.brightText,
                    ),
                  ),
                ),
              ),
              TranscriptGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter),
              Align(
                alignment: Alignment.bottomCenter,
                child: TranscriptGradient(begin: Alignment.bottomCenter, end: Alignment.topCenter),
              ),
            ],
          ),
        ),
        InkWell(
          onTap: () {
            isTranscriptShown.value = !isTranscriptShown.value;
          },
          child: Container(
            width: double.infinity,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: Text(
              isTranscriptShown.value ? 'Скрыть расшифровку' : 'Показать расшифровку',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
