import 'package:audioplayer/app_colors.dart';
import 'package:audioplayer/models/audio_message.dart';
import 'package:audioplayer/screens/home/widgets/playback_controls.dart';
import 'package:audioplayer/screens/home/widgets/transcript.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'media_controls.dart';

class PlayerModal extends StatelessWidget {
  const PlayerModal({required this.message});

  final AudioMessage message;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        const SizedBox(height: 8),
        Container(
          width: 32,
          height: 4,
          decoration: BoxDecoration(
            color: AppColors.base,
            borderRadius: BorderRadius.circular(2),
          ),
        ),
        const SizedBox(height: 16),
        Text(
          message.title,
          style: TextStyle(
            color: AppColors.darkText,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 8),
        Text(
          DateFormat('d MMMM в HH:mm', 'ru').format(message.creationDate),
          style: TextStyle(color: AppColors.brightText),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: MessageTranscript(message: message),
        ),
        const SizedBox(height: 24),
        PlaybackControls(message: message),
        const SizedBox(height: 28),
        MediaControls(),
        const SizedBox(height: 36),
      ]),
    );
  }
}
