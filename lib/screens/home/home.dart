import 'package:audioplayer/models/audio_message.dart';
import 'package:audioplayer/screens/home/widgets/player_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({required this.messages});

  final List<AudioMessage> messages;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Audioplayer demo'),
      ),
      body: ListView.builder(
        itemCount: messages.length,
        itemBuilder: (context, index) {
          final message = messages[index];

          return ListTile(
            leading: CircleAvatar(
              radius: 13,
              child: Icon(Icons.play_arrow_outlined, size: 24),
            ),
            isThreeLine: true,
            title: Text(message.title),
            subtitle: Text(
              message.transcript,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () {
              showCupertinoModalBottomSheet(
                context: context,
                builder: (context) => PlayerModal(message: message),
              );
            },
          );
        },
      ),
    );
  }
}
