class AppAssets {
  static const add = 'assets/add.svg';
  static const download = 'assets/download.svg';
  static const reply = 'assets/reply.svg';
  static const speechBubble = 'assets/speech_bubble.svg';
}
